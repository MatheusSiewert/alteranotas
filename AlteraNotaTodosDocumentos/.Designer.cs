﻿namespace AlteraNotaTodosDocumentos
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Gerar = new System.Windows.Forms.Button();
            this.Inicio = new System.Windows.Forms.Label();
            this.Fim = new System.Windows.Forms.Label();
            this.txtDiretorio = new System.Windows.Forms.TextBox();
            this.lblDiretorio = new System.Windows.Forms.Label();
            this.cbxDocumento = new System.Windows.Forms.ComboBox();
            this.lblTipoDocumento = new System.Windows.Forms.Label();
            this.btnSelecionarDiretorio = new System.Windows.Forms.Button();
            this.txbInicio = new System.Windows.Forms.MaskedTextBox();
            this.txbFim = new System.Windows.Forms.MaskedTextBox();
            this.lblDocumentoSelecionado = new System.Windows.Forms.Label();
            this.txbXmlUtilizado = new System.Windows.Forms.TextBox();
            this.btnXmlUtilizado = new System.Windows.Forms.Button();
            this.lblCnpj = new System.Windows.Forms.Label();
            this.lblIncricaoMunicipal = new System.Windows.Forms.Label();
            this.txbCnpj = new System.Windows.Forms.TextBox();
            this.txbInscricaoMunicipal = new System.Windows.Forms.TextBox();
            this.txbInscricaoEstadual = new System.Windows.Forms.TextBox();
            this.lblInscricaoEstadual = new System.Windows.Forms.Label();
            this.txbSerie = new System.Windows.Forms.TextBox();
            this.lblSerie = new System.Windows.Forms.Label();
            this.cbxTipoOperacao = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // Gerar
            // 
            this.Gerar.Location = new System.Drawing.Point(239, 251);
            this.Gerar.Name = "Gerar";
            this.Gerar.Size = new System.Drawing.Size(75, 23);
            this.Gerar.TabIndex = 0;
            this.Gerar.Text = "Gerar";
            this.Gerar.UseVisualStyleBackColor = true;
            this.Gerar.Click += new System.EventHandler(this.Gerar_Click);
            // 
            // Inicio
            // 
            this.Inicio.AutoSize = true;
            this.Inicio.Location = new System.Drawing.Point(65, 21);
            this.Inicio.Name = "Inicio";
            this.Inicio.Size = new System.Drawing.Size(39, 13);
            this.Inicio.TabIndex = 2;
            this.Inicio.Text = "Inicio *";
            // 
            // Fim
            // 
            this.Fim.AutoSize = true;
            this.Fim.Location = new System.Drawing.Point(74, 48);
            this.Fim.Name = "Fim";
            this.Fim.Size = new System.Drawing.Size(30, 13);
            this.Fim.TabIndex = 4;
            this.Fim.Text = "Fim *";
            // 
            // txtDiretorio
            // 
            this.txtDiretorio.Location = new System.Drawing.Point(105, 69);
            this.txtDiretorio.Name = "txtDiretorio";
            this.txtDiretorio.Size = new System.Drawing.Size(352, 20);
            this.txtDiretorio.TabIndex = 6;
            // 
            // lblDiretorio
            // 
            this.lblDiretorio.AutoSize = true;
            this.lblDiretorio.Location = new System.Drawing.Point(-2, 71);
            this.lblDiretorio.Name = "lblDiretorio";
            this.lblDiretorio.Size = new System.Drawing.Size(107, 13);
            this.lblDiretorio.TabIndex = 7;
            this.lblDiretorio.Text = "Diretório a ser salvo *";
            // 
            // cbxDocumento
            // 
            this.cbxDocumento.FormattingEnabled = true;
            this.cbxDocumento.Items.AddRange(new object[] {
            "NF-e",
            "NFS-e",
            "CT-e",
            "MDF-e",
            "NFC-e",
            "CF-e",
            "eSocial",
            "EFD - Reinf",
            "Bloco X"});
            this.cbxDocumento.Location = new System.Drawing.Point(105, 94);
            this.cbxDocumento.Name = "cbxDocumento";
            this.cbxDocumento.Size = new System.Drawing.Size(352, 21);
            this.cbxDocumento.TabIndex = 8;
            // 
            // lblTipoDocumento
            // 
            this.lblTipoDocumento.AutoSize = true;
            this.lblTipoDocumento.Location = new System.Drawing.Point(34, 97);
            this.lblTipoDocumento.Name = "lblTipoDocumento";
            this.lblTipoDocumento.Size = new System.Drawing.Size(69, 13);
            this.lblTipoDocumento.TabIndex = 9;
            this.lblTipoDocumento.Text = "Documento *";
            // 
            // btnSelecionarDiretorio
            // 
            this.btnSelecionarDiretorio.Location = new System.Drawing.Point(457, 68);
            this.btnSelecionarDiretorio.Name = "btnSelecionarDiretorio";
            this.btnSelecionarDiretorio.Size = new System.Drawing.Size(117, 23);
            this.btnSelecionarDiretorio.TabIndex = 10;
            this.btnSelecionarDiretorio.Text = "Selecionar o diretório";
            this.btnSelecionarDiretorio.UseVisualStyleBackColor = true;
            this.btnSelecionarDiretorio.Click += new System.EventHandler(this.btnSelecionarDiretorio_Click);
            // 
            // txbInicio
            // 
            this.txbInicio.Location = new System.Drawing.Point(106, 19);
            this.txbInicio.Name = "txbInicio";
            this.txbInicio.PromptChar = ' ';
            this.txbInicio.Size = new System.Drawing.Size(351, 20);
            this.txbInicio.TabIndex = 11;
            this.txbInicio.Text = "0";
            // 
            // txbFim
            // 
            this.txbFim.Location = new System.Drawing.Point(106, 44);
            this.txbFim.Name = "txbFim";
            this.txbFim.PromptChar = ' ';
            this.txbFim.ResetOnSpace = false;
            this.txbFim.Size = new System.Drawing.Size(351, 20);
            this.txbFim.TabIndex = 12;
            this.txbFim.Text = "0";
            // 
            // lblDocumentoSelecionado
            // 
            this.lblDocumentoSelecionado.AutoSize = true;
            this.lblDocumentoSelecionado.Location = new System.Drawing.Point(26, 120);
            this.lblDocumentoSelecionado.Name = "lblDocumentoSelecionado";
            this.lblDocumentoSelecionado.Size = new System.Drawing.Size(70, 13);
            this.lblDocumentoSelecionado.TabIndex = 13;
            this.lblDocumentoSelecionado.Text = "XML utilizado";
            // 
            // txbXmlUtilizado
            // 
            this.txbXmlUtilizado.Location = new System.Drawing.Point(105, 120);
            this.txbXmlUtilizado.Name = "txbXmlUtilizado";
            this.txbXmlUtilizado.Size = new System.Drawing.Size(352, 20);
            this.txbXmlUtilizado.TabIndex = 14;
            // 
            // btnXmlUtilizado
            // 
            this.btnXmlUtilizado.Location = new System.Drawing.Point(457, 118);
            this.btnXmlUtilizado.Name = "btnXmlUtilizado";
            this.btnXmlUtilizado.Size = new System.Drawing.Size(117, 23);
            this.btnXmlUtilizado.TabIndex = 15;
            this.btnXmlUtilizado.Text = "Selecionar o diretório";
            this.btnXmlUtilizado.UseVisualStyleBackColor = true;
            this.btnXmlUtilizado.Click += new System.EventHandler(this.btnXmlUtilizado_Click);
            // 
            // lblCnpj
            // 
            this.lblCnpj.AutoSize = true;
            this.lblCnpj.Location = new System.Drawing.Point(56, 148);
            this.lblCnpj.Name = "lblCnpj";
            this.lblCnpj.Size = new System.Drawing.Size(34, 13);
            this.lblCnpj.TabIndex = 16;
            this.lblCnpj.Text = "CNPJ";
            // 
            // lblIncricaoMunicipal
            // 
            this.lblIncricaoMunicipal.AutoSize = true;
            this.lblIncricaoMunicipal.Location = new System.Drawing.Point(6, 174);
            this.lblIncricaoMunicipal.Name = "lblIncricaoMunicipal";
            this.lblIncricaoMunicipal.Size = new System.Drawing.Size(90, 13);
            this.lblIncricaoMunicipal.TabIndex = 17;
            this.lblIncricaoMunicipal.Text = "IncricaoMunicipal";
            // 
            // txbCnpj
            // 
            this.txbCnpj.Location = new System.Drawing.Point(105, 146);
            this.txbCnpj.Name = "txbCnpj";
            this.txbCnpj.Size = new System.Drawing.Size(352, 20);
            this.txbCnpj.TabIndex = 18;
            // 
            // txbInscricaoMunicipal
            // 
            this.txbInscricaoMunicipal.Location = new System.Drawing.Point(105, 171);
            this.txbInscricaoMunicipal.Name = "txbInscricaoMunicipal";
            this.txbInscricaoMunicipal.Size = new System.Drawing.Size(352, 20);
            this.txbInscricaoMunicipal.TabIndex = 19;
            // 
            // txbInscricaoEstadual
            // 
            this.txbInscricaoEstadual.Location = new System.Drawing.Point(105, 197);
            this.txbInscricaoEstadual.Name = "txbInscricaoEstadual";
            this.txbInscricaoEstadual.Size = new System.Drawing.Size(352, 20);
            this.txbInscricaoEstadual.TabIndex = 21;
            // 
            // lblInscricaoEstadual
            // 
            this.lblInscricaoEstadual.AutoSize = true;
            this.lblInscricaoEstadual.Location = new System.Drawing.Point(6, 200);
            this.lblInscricaoEstadual.Name = "lblInscricaoEstadual";
            this.lblInscricaoEstadual.Size = new System.Drawing.Size(86, 13);
            this.lblInscricaoEstadual.TabIndex = 20;
            this.lblInscricaoEstadual.Text = "IncricaoEstadual";
            // 
            // txbSerie
            // 
            this.txbSerie.Location = new System.Drawing.Point(105, 222);
            this.txbSerie.Name = "txbSerie";
            this.txbSerie.Size = new System.Drawing.Size(352, 20);
            this.txbSerie.TabIndex = 23;
            // 
            // lblSerie
            // 
            this.lblSerie.AutoSize = true;
            this.lblSerie.Location = new System.Drawing.Point(59, 222);
            this.lblSerie.Name = "lblSerie";
            this.lblSerie.Size = new System.Drawing.Size(31, 13);
            this.lblSerie.TabIndex = 22;
            this.lblSerie.Text = "Serie";
            // 
            // cbxTipoOperacao
            // 
            this.cbxTipoOperacao.FormattingEnabled = true;
            this.cbxTipoOperacao.Items.AddRange(new object[] {
            "Emissão",
            "Carta de correção"});
            this.cbxTipoOperacao.Location = new System.Drawing.Point(464, 93);
            this.cbxTipoOperacao.Name = "cbxTipoOperacao";
            this.cbxTipoOperacao.Size = new System.Drawing.Size(110, 21);
            this.cbxTipoOperacao.TabIndex = 24;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(578, 279);
            this.Controls.Add(this.cbxTipoOperacao);
            this.Controls.Add(this.txbSerie);
            this.Controls.Add(this.lblSerie);
            this.Controls.Add(this.txbInscricaoEstadual);
            this.Controls.Add(this.lblInscricaoEstadual);
            this.Controls.Add(this.txbInscricaoMunicipal);
            this.Controls.Add(this.txbCnpj);
            this.Controls.Add(this.lblIncricaoMunicipal);
            this.Controls.Add(this.lblCnpj);
            this.Controls.Add(this.btnXmlUtilizado);
            this.Controls.Add(this.txbXmlUtilizado);
            this.Controls.Add(this.lblDocumentoSelecionado);
            this.Controls.Add(this.txbFim);
            this.Controls.Add(this.txbInicio);
            this.Controls.Add(this.btnSelecionarDiretorio);
            this.Controls.Add(this.lblTipoDocumento);
            this.Controls.Add(this.cbxDocumento);
            this.Controls.Add(this.lblDiretorio);
            this.Controls.Add(this.txtDiretorio);
            this.Controls.Add(this.Fim);
            this.Controls.Add(this.Inicio);
            this.Controls.Add(this.Gerar);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Gerar;
        private System.Windows.Forms.Label Inicio;
        private System.Windows.Forms.Label Fim;
        private System.Windows.Forms.TextBox txtDiretorio;
        private System.Windows.Forms.Label lblDiretorio;
        private System.Windows.Forms.ComboBox cbxDocumento;
        private System.Windows.Forms.Label lblTipoDocumento;
        private System.Windows.Forms.Button btnSelecionarDiretorio;
        private System.Windows.Forms.MaskedTextBox txbInicio;
        private System.Windows.Forms.MaskedTextBox txbFim;
        private System.Windows.Forms.Label lblDocumentoSelecionado;
        private System.Windows.Forms.TextBox txbXmlUtilizado;
        private System.Windows.Forms.Button btnXmlUtilizado;
        private System.Windows.Forms.Label lblCnpj;
        private System.Windows.Forms.Label lblIncricaoMunicipal;
        private System.Windows.Forms.TextBox txbCnpj;
        private System.Windows.Forms.TextBox txbInscricaoMunicipal;
        private System.Windows.Forms.TextBox txbInscricaoEstadual;
        private System.Windows.Forms.Label lblInscricaoEstadual;
        private System.Windows.Forms.TextBox txbSerie;
        private System.Windows.Forms.Label lblSerie;
        private System.Windows.Forms.ComboBox cbxTipoOperacao;
    }
}

