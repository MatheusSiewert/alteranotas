﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Threading;
using System.Windows.Forms;
using System.Xml;

namespace AlteraNotaTodosDocumentos
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Gerar_Click(object sender, EventArgs e)
        {
            try
            {
                var teste = Convert.ToInt32(txbInicio.Text);
                teste = Convert.ToInt32(txbFim.Text);
            }
            catch (Exception)
            {
                MessageBox.Show("Insira apenas numeros");
                return;
            }
            var inicio = txbInicio.Text;
            var fim = txbFim.Text;

            var itemSelecionado = cbxDocumento.SelectedIndex;

            switch (itemSelecionado)
            {
                //NF-e
                case 0:
                    switch (cbxTipoOperacao.SelectedIndex)
                    {
                        case 1:
                            GeradorNfeCce(inicio, fim);
                            break;

                        default:
                            GeradorNFe(inicio, fim);
                            break;
                    }
                    break;

                //NFS-e
                case 1:
                    GeradorNFSe(inicio, fim);
                    break;

                //CT-e
                case 2:
                    GeradorCte(inicio, fim);
                    break;

                //MDF-e
                case 3:
                    GeradorMdfe(inicio, fim);
                    break;

                case 4:
                    GeradorNfce(inicio, fim);
                    break;

                default:
                    return;
            }
        }

        private void GeradorNFe(string inicial, string final)
        {
            var nfe = new XmlDocument();
            if (!String.IsNullOrEmpty(txbXmlUtilizado.Text))
            {
                nfe.Load(txbXmlUtilizado.Text);
            }
            else
            {
                nfe.LoadXml(XmlNfe());
            }

            var contador = Convert.ToInt32(inicial);
            var ultimoNumero = Convert.ToInt32(final);
            nfe.GetElementsByTagName("dhEmi")[0].InnerText = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:sszzz");
            nfe.GetElementsByTagName("dhSaiEnt")[0].InnerText = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:sszzz");
            nfe.GetElementsByTagName("dVenc")[0].InnerText = DateTime.Now.AddDays(2).ToString("yyyy-MM-dd");
            nfe.GetElementsByTagName("serie")[0].InnerText = String.IsNullOrEmpty(txbSerie.Text) ? nfe.GetElementsByTagName("serie")[0].InnerText : txbSerie.Text;
            nfe.GetElementsByTagName("IE")[0].InnerText = String.IsNullOrEmpty(txbInscricaoEstadual.Text) ? nfe.GetElementsByTagName("IE")[0].InnerText : txbInscricaoEstadual.Text;
            nfe.GetElementsByTagName("IM")[0].InnerText = String.IsNullOrEmpty(txbInscricaoMunicipal.Text) ? nfe.GetElementsByTagName("IM")[0].InnerText : txbInscricaoMunicipal.Text;
            nfe.GetElementsByTagName("CNPJ")[0].InnerText = String.IsNullOrEmpty(txbCnpj.Text) ? nfe.GetElementsByTagName("CNPJ")[0].InnerText : txbCnpj.Text;

            var chaveGeral = nfe.GetElementsByTagName("cUF")[0].InnerText + DateTime.Now.ToString("yy") + DateTime.Now.ToString("MM") + nfe.GetElementsByTagName("CNPJ")[0].InnerText + "55" + Convert.ToInt32(nfe.GetElementsByTagName("serie")[0].InnerText).ToString("000");

            Int64 contadorPreco = 0;

            while (contador <= ultimoNumero)
            {
                //alteração feita apenas para testes, muda todos os valores da nota
                nfe.GetElementsByTagName("xProd")[0].InnerText = "Nota gerada: " + contadorPreco.ToString();
                nfe.GetElementsByTagName("qCom")[0].InnerText = contadorPreco.ToString() + ".000";
                nfe.GetElementsByTagName("vUnCom")[0].InnerText = contadorPreco.ToString() + ".000";
                nfe.GetElementsByTagName("vProd")[0].InnerText = (contadorPreco * contadorPreco).ToString() + ".00";
                nfe.GetElementsByTagName("vProd")[1].InnerText = (contadorPreco * contadorPreco).ToString() + ".00";
                nfe.GetElementsByTagName("qTrib")[0].InnerText = contadorPreco.ToString() + ".0000";
                nfe.GetElementsByTagName("vUnTrib")[0].InnerText = contadorPreco.ToString() + ".0000000000";
                nfe.GetElementsByTagName("vNF")[0].InnerText = (contadorPreco * contadorPreco).ToString() + ".00";
                nfe.GetElementsByTagName("vOrig")[0].InnerText = (contadorPreco * contadorPreco).ToString() + ".00";
                nfe.GetElementsByTagName("vLiq")[0].InnerText = (contadorPreco * contadorPreco).ToString() + ".00";
                nfe.GetElementsByTagName("vDup")[0].InnerText = (contadorPreco * contadorPreco).ToString() + ".00";
                nfe.GetElementsByTagName("vPag")[0].InnerText = (contadorPreco * contadorPreco).ToString() + ".00";

                nfe.GetElementsByTagName("nNF")[0].InnerText = contador.ToString();
                var chave = chaveGeral + contador.ToString("000000000");
                chave += "1";
                chave += nfe.GetElementsByTagName("cNF")[0].InnerText;
                nfe.DocumentElement.GetElementsByTagName("infNFe")[0].Attributes["Id"].Value = "NFe" + chave + GerarDigitoVerificadorNFe(chave);
                nfe.GetElementsByTagName("cDV")[0].InnerText = GerarDigitoVerificadorNFe(chave);

                nfe.Save(txtDiretorio.Text + "/NFeNumero" + contador + ".xml");

                contador++;
                contadorPreco++;
            }
        }

        private void GeradorNfeCce(string inicial, string final)
        {
            var cce = new XmlDocument();
            if (!String.IsNullOrEmpty(txbXmlUtilizado.Text))
            {
                cce.Load(txbXmlUtilizado.Text);
            }
            else
            {
                cce.LoadXml(XmlNfeCce());
            }

            var chaveXml = cce.ChildNodes[1].FirstChild.Attributes[0].InnerText;

            cce.ChildNodes[1].FirstChild.ChildNodes[2].InnerText = String.IsNullOrEmpty(txbCnpj.Text) ? cce.ChildNodes[1].FirstChild.ChildNodes[2].InnerText : txbCnpj.Text;
            cce.GetElementsByTagName("dhEvento")[0].InnerText = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:sszzz");
            var sla = cce.ChildNodes[1].FirstChild.ChildNodes[2].InnerText;
            var chave = cce.ChildNodes[1].FirstChild.ChildNodes[0].InnerText + DateTime.Now.ToString("yy") + DateTime.Now.ToString("MM") + cce.GetElementsByTagName("CNPJ")[0].InnerText + "55" + chaveXml.Substring(30, 3);

            var contador = Convert.ToInt32(inicial);
            var ultimoNumero = Convert.ToInt32(final);

            while (contador <= ultimoNumero)
            {
                var parteChave = chave + contador.ToString("000000000") + "1" + chaveXml.Substring(43, 9);
                cce.ChildNodes[1].FirstChild.Attributes[0].InnerText = "ID110110" + parteChave + "01";
                cce.ChildNodes[1].FirstChild.ChildNodes[3].InnerText = parteChave;

                cce.Save(txtDiretorio.Text + "/NFCeNumero" + contador + ".xml");

                contador++;
            }
        }

        private void GeradorNFSe(string inicial, string final)
        {
            var nfse = new XmlDocument();
            if (!String.IsNullOrEmpty(txbXmlUtilizado.Text))
            {
                nfse.Load(txbXmlUtilizado.Text);
            }
            else
            {
                nfse.LoadXml(XmlNfse());
            }
            //cnpj
            nfse.ChildNodes[1].FirstChild.ChildNodes[1].InnerText = String.IsNullOrEmpty(txbCnpj.Text) ? nfse.ChildNodes[1].FirstChild.ChildNodes[1].InnerText : txbCnpj.Text;
            nfse.ChildNodes[1].FirstChild.LastChild.FirstChild.FirstChild.ChildNodes[8].FirstChild.InnerText = String.IsNullOrEmpty(txbCnpj.Text) ? nfse.ChildNodes[1].FirstChild.LastChild.FirstChild.FirstChild.ChildNodes[8].FirstChild.InnerText : txbCnpj.Text;
            //inscricaoMunicipal
            nfse.ChildNodes[1].FirstChild.ChildNodes[2].InnerText = String.IsNullOrEmpty(txbInscricaoMunicipal.Text) ? nfse.ChildNodes[1].FirstChild.ChildNodes[2].InnerText : txbInscricaoMunicipal.Text;
            nfse.ChildNodes[1].FirstChild.LastChild.FirstChild.FirstChild.ChildNodes[8].LastChild.InnerText = String.IsNullOrEmpty(txbInscricaoMunicipal.Text) ? nfse.ChildNodes[1].FirstChild.LastChild.FirstChild.FirstChild.ChildNodes[8].LastChild.InnerText : txbInscricaoMunicipal.Text;
            //serie
            nfse.ChildNodes[1].FirstChild.LastChild.FirstChild.FirstChild.ChildNodes[0].ChildNodes[1].InnerText = String.IsNullOrEmpty(txbSerie.Text) ? nfse.ChildNodes[1].FirstChild.LastChild.FirstChild.FirstChild.ChildNodes[0].ChildNodes[1].InnerText : txbSerie.Text;
            //dataEmissao
            nfse.ChildNodes[1].FirstChild.LastChild.FirstChild.FirstChild.ChildNodes[1].InnerText = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:sszzz");

            var contador = Convert.ToInt32(inicial);
            var ultimoNumero = Convert.ToInt32(final);

            while (contador <= ultimoNumero)
            {
                nfse.ChildNodes[1].FirstChild.LastChild.FirstChild.FirstChild.ChildNodes[0].ChildNodes[0].InnerText = contador.ToString();

                nfse.Save(txtDiretorio.Text + "/NFSeNumero" + contador + ".xml");

                contador++;
            }
        }

        private void GeradorCte(string inicial, string final)
        {
            var cte = new XmlDocument();
            if (!String.IsNullOrEmpty(txbXmlUtilizado.Text))
            {
                cte.Load(txbXmlUtilizado.Text);
            }
            else
            {
                cte.LoadXml(XmlCte());
            }

            var contador = Convert.ToInt32(inicial);
            var ultimoNumero = Convert.ToInt32(final);
            cte.GetElementsByTagName("dhEmi")[0].InnerText = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:sszzz");
            cte.GetElementsByTagName("serie")[0].InnerText = String.IsNullOrEmpty(txbSerie.Text) ? cte.GetElementsByTagName("serie")[0].InnerText : txbSerie.Text;
            cte.GetElementsByTagName("IE")[0].InnerText = String.IsNullOrEmpty(txbInscricaoEstadual.Text) ? cte.GetElementsByTagName("IE")[0].InnerText : txbInscricaoEstadual.Text;
            cte.GetElementsByTagName("CNPJ")[0].InnerText = String.IsNullOrEmpty(txbCnpj.Text) ? cte.GetElementsByTagName("CNPJ")[0].InnerText : txbCnpj.Text;

            var chaveGeral = cte.GetElementsByTagName("cUF")[0].InnerText + DateTime.Now.ToString("yy") + DateTime.Now.ToString("MM") + cte.GetElementsByTagName("CNPJ")[0].InnerText + "57" + Convert.ToInt32(cte.GetElementsByTagName("serie")[0].InnerText).ToString("000");

            while (contador <= ultimoNumero)
            {
                cte.GetElementsByTagName("nCT")[0].InnerText = contador.ToString();
                var chave = chaveGeral + contador.ToString("000000000");
                chave += "1";
                chave += cte.GetElementsByTagName("cCT")[0].InnerText;
                cte.DocumentElement.GetElementsByTagName("infCte")[0].Attributes["Id"].Value = "CTe" + chave + GerarDigitoVerificadorNFe(chave);
                cte.GetElementsByTagName("cDV")[0].InnerText = GerarDigitoVerificadorNFe(chave);

                cte.Save(txtDiretorio.Text + "/CteNumero" + contador + ".xml");

                contador++;
            }
        }

        private void GeradorMdfe(string inicial, string final)
        {
            var mdfe = new XmlDocument();
            if (!String.IsNullOrEmpty(txbXmlUtilizado.Text))
            {
                mdfe.Load(txbXmlUtilizado.Text);
            }
            else
            {
                mdfe.LoadXml(XmlMdfe());
            }

            var contador = Convert.ToInt32(inicial);
            var ultimoNumero = Convert.ToInt32(final);
            mdfe.GetElementsByTagName("dhEmi")[0].InnerText = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:sszzz");
            mdfe.GetElementsByTagName("dhIniViagem")[0].InnerText = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:sszzz");
            mdfe.GetElementsByTagName("serie")[0].InnerText = String.IsNullOrEmpty(txbSerie.Text) ? mdfe.GetElementsByTagName("serie")[0].InnerText : Convert.ToInt32(txbSerie.Text).ToString("000");
            mdfe.GetElementsByTagName("IE")[0].InnerText = String.IsNullOrEmpty(txbInscricaoEstadual.Text) ? mdfe.GetElementsByTagName("IE")[0].InnerText : txbInscricaoEstadual.Text;
            mdfe.GetElementsByTagName("CNPJ")[0].InnerText = String.IsNullOrEmpty(txbCnpj.Text) ? mdfe.GetElementsByTagName("CNPJ")[0].InnerText : txbCnpj.Text;

            var chaveGeral = mdfe.GetElementsByTagName("cUF")[0].InnerText + DateTime.Now.ToString("yy") + DateTime.Now.ToString("MM") + mdfe.GetElementsByTagName("CNPJ")[0].InnerText + "58" + Convert.ToInt32(mdfe.GetElementsByTagName("serie")[0].InnerText).ToString("000");

            while (contador <= ultimoNumero)
            {
                mdfe.GetElementsByTagName("nMDF")[0].InnerText = contador.ToString();
                var chave = chaveGeral + contador.ToString("000000000");
                chave += "1";
                chave += mdfe.GetElementsByTagName("cMDF")[0].InnerText;
                mdfe.DocumentElement.GetElementsByTagName("infMDFe")[0].Attributes["Id"].Value = "MDFe" + chave + GerarDigitoVerificadorNFe(chave);
                mdfe.GetElementsByTagName("cDV")[0].InnerText = GerarDigitoVerificadorNFe(chave);

                mdfe.Save(txtDiretorio.Text + "/MdfeNumero" + contador + ".xml");

                contador++;
            }
        }

        private void GeradorNfce(string inicial, string final)
        {
            var nfce = new XmlDocument();
            if (!String.IsNullOrEmpty(txbXmlUtilizado.Text))
            {
                nfce.Load(txbXmlUtilizado.Text);
            }
            else
            {
                nfce.LoadXml(XmlNfce());
            }

            var contador = Convert.ToInt32(inicial);
            var ultimoNumero = Convert.ToInt32(final);
            nfce.GetElementsByTagName("dhEmi")[0].InnerText = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:sszzz");
            //nfce.GetElementsByTagName("dhIniViagem")[0].InnerText = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:sszzz");
            nfce.GetElementsByTagName("serie")[0].InnerText = String.IsNullOrEmpty(txbSerie.Text) ? nfce.GetElementsByTagName("serie")[0].InnerText : Convert.ToInt32(txbSerie.Text).ToString("000");
            nfce.GetElementsByTagName("IE")[0].InnerText = String.IsNullOrEmpty(txbInscricaoEstadual.Text) ? nfce.GetElementsByTagName("IE")[0].InnerText : txbInscricaoEstadual.Text;
            nfce.GetElementsByTagName("CNPJ")[0].InnerText = String.IsNullOrEmpty(txbCnpj.Text) ? nfce.GetElementsByTagName("CNPJ")[0].InnerText : txbCnpj.Text;

            var chaveGeral = nfce.GetElementsByTagName("cUF")[0].InnerText + DateTime.Now.ToString("yy") + DateTime.Now.ToString("MM") + nfce.GetElementsByTagName("CNPJ")[0].InnerText + "58" + Convert.ToInt32(nfce.GetElementsByTagName("serie")[0].InnerText).ToString("000");

            while (contador <= ultimoNumero)
            {
                nfce.GetElementsByTagName("nMDF")[0].InnerText = contador.ToString();
                var chave = chaveGeral + contador.ToString("000000000");
                chave += "1";
                chave += nfce.GetElementsByTagName("cMDF")[0].InnerText;
                nfce.DocumentElement.GetElementsByTagName("infNFe")[0].Attributes["Id"].Value = "NFe" + chave + GerarDigitoVerificadorNFe(chave);
                nfce.GetElementsByTagName("cDV")[0].InnerText = GerarDigitoVerificadorNFe(chave);

                nfce.Save(txtDiretorio.Text + "/MdfeNumero" + contador + ".xml");

                contador++;
            }
        }

        private string XmlNfe()
        {
            return "<?xml version=\"1.0\" encoding=\"UTF-8\"?><NFe xmlns=\"http://www.portalfiscal.inf.br/nfe\"><infNFe Id=\"NFe42190180680093000181551220000059791800721404\" versao=\"4.00\"><ide><cUF>42</cUF><cNF>80072140</cNF><natOp>Venda Mercad.Adquir.e/ou Receb.Terceiros</natOp><mod>55</mod><serie>122</serie><nNF>5979</nNF><dhEmi>2019-01-03T10:53:00-02:00</dhEmi><dhSaiEnt>2019-01-03T10:53:00-02:00</dhSaiEnt><tpNF>1</tpNF><idDest>1</idDest><cMunFG>4202404</cMunFG><tpImp>1</tpImp><tpEmis>1</tpEmis><cDV>4</cDV><tpAmb>2</tpAmb><finNFe>1</finNFe><indFinal>1</indFinal><indPres>1</indPres><procEmi>0</procEmi><verProc>5.8.10.95</verProc></ide><emit><CNPJ>80680093000181</CNPJ><xNome>Senior Sistemas</xNome><xFant>Senior Sistemas SA</xFant><enderEmit><xLgr>Rua Sao Paulo</xLgr><nro>825</nro><xCpl>Sede Nova</xCpl><xBairro>Victor Konder</xBairro><cMun>4202404</cMun><xMun>BLUMENAU</xMun><UF>SC</UF><CEP>89012530</CEP><cPais>1058</cPais><xPais>Brasil</xPais><fone>47333403300</fone></enderEmit><IE>252812441</IE><IM>ISENTO</IM><CNAE>6504000</CNAE><CRT>3</CRT></emit><dest><CNPJ>99999999000191</CNPJ><xNome>NF-E EMITIDA EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL</xNome><enderDest><xLgr>Rua dos testes</xLgr><nro>123</nro><xCpl>1234</xCpl><xBairro>Centro</xBairro><cMun>4202404</cMun><xMun>BLUMENAU</xMun><UF>SC</UF><CEP>89000000</CEP><cPais>1058</cPais><xPais>Brasil</xPais></enderDest><indIEDest>9</indIEDest><email>alan.voigt@senior.com.br</email></dest><det nItem=\"1\"><prod><cProd>COMB01</cProd><cEAN>SEM GTIN</cEAN><xProd>M 91% POLIAMIDA 9% ELASTANO CHINA 144 g/m² 18 CM Aditivado</xProd><NCM>27101259</NCM><CFOP>5102</CFOP><uCom>ML</uCom><qCom>4.0000</qCom><vUnCom>3.0000000000</vUnCom><vProd>12.00</vProd><cEANTrib>SEM GTIN</cEANTrib><uTrib>ML</uTrib><qTrib>4.0000</qTrib><vUnTrib>3.0000000000</vUnTrib><indTot>1</indTot><comb><cProdANP>320102002</cProdANP><descANP>teste</descANP><UFCons>SC</UFCons></comb></prod><imposto><ICMS><ICMS40><orig>0</orig><CST>40</CST></ICMS40></ICMS><IPI><cEnq>999</cEnq><IPITrib><CST>50</CST><vBC>0</vBC><pIPI>0</pIPI><vIPI>0</vIPI></IPITrib></IPI><PIS><PISNT><CST>06</CST></PISNT></PIS><COFINS><COFINSNT><CST>06</CST></COFINSNT></COFINS></imposto></det><total><ICMSTot><vBC>0.00</vBC><vICMS>0.00</vICMS><vICMSDeson>0.00</vICMSDeson><vFCP>0.00</vFCP><vBCST>0.00</vBCST><vST>0.00</vST><vFCPST>0.00</vFCPST><vFCPSTRet>0.00</vFCPSTRet><vProd>12.00</vProd><vFrete>0.00</vFrete><vSeg>0.00</vSeg><vDesc>0.00</vDesc><vII>0.00</vII><vIPI>0.00</vIPI><vIPIDevol>0.00</vIPIDevol><vPIS>0.00</vPIS><vCOFINS>0.00</vCOFINS><vOutro>0.00</vOutro><vNF>12.00</vNF></ICMSTot></total><transp><modFrete>0</modFrete></transp><cobr><fat><nFat>5979</nFat><vOrig>12.00</vOrig><vDesc>0.00</vDesc><vLiq>12.00</vLiq></fat><dup><nDup>001</nDup><dVenc>2019-01-03</dVenc><vDup>12.00</vDup></dup></cobr><pag><detPag><tPag>99</tPag><vPag>12.00</vPag></detPag></pag></infNFe></NFe>";
        }

        private string XmlNfeCce()
        {
            return "<?xml version=\"1.0\" encoding=\"UTF-8\"?><evento versao=\"1.00\" xmlns=\"http://www.portalfiscal.inf.br/nfe\"><infEvento Id=\"ID1101104219018068009300018155122000005986100041707101\"><cOrgao>42</cOrgao><tpAmb>2</tpAmb><CNPJ>80680093000181</CNPJ><chNFe>42190180680093000181551220000059861000417071</chNFe><dhEvento>2019-01-04T15:41:00-02:00</dhEvento><tpEvento>110110</tpEvento><nSeqEvento>1</nSeqEvento><verEvento>1.00</verEvento><detEvento versao=\"1.00\"><descEvento>Carta de Correcao</descEvento><xCorrecao>teste teste teste teste.</xCorrecao><xCondUso>A Carta de Correcao e disciplinada pelo paragrafo 1o-A do art. 7o do Convenio S/N, de 15 de dezembro de 1970 e pode ser utilizada para regularizacao de erro ocorrido na emissao de documento fiscal, desde que o erro nao esteja relacionado com: I - as variaveis que determinam o valor do imposto tais como: base de calculo, aliquota, diferenca de preco, quantidade, valor da operacao ou da prestacao; II - a correcao de dados cadastrais que implique mudanca do remetente ou do destinatario; III - a data de emissao ou de saida.</xCondUso></detEvento></infEvento></evento>";
        }

        private string XmlNfse()
        {
            return "<?xml version=\"1.0\" encoding=\"UTF-8\"?><EnviarLoteRpsEnvio xmlns=\"http://www.abrasf.org.br/ABRASF/arquivos/nfse.xsd\"><LoteRps id=\"ID17047773008610001100000001100000011000000010\"><NumeroLote>000000000000001</NumeroLote><Cnpj>00000000000000</Cnpj><InscricaoMunicipal>00000</InscricaoMunicipal><QuantidadeRps>1</QuantidadeRps><ListaRps><Rps><InfRps id=\"ID41170477300861000110551200000100030000000094\"><IdentificacaoRps><Numero>10003</Numero><Serie>120</Serie><Tipo>1</Tipo></IdentificacaoRps><DataEmissao>2018-04-01T08:00:00</DataEmissao><NaturezaOperacao>5</NaturezaOperacao><RegimeEspecialTributacao>6</RegimeEspecialTributacao><OptanteSimplesNacional>2</OptanteSimplesNacional><IncentivadorCultural>2</IncentivadorCultural><Status>1</Status><Servico><Valores><ValorServicos>1.00</ValorServicos><IssRetido>2</IssRetido><ValorIss>0.05</ValorIss><BaseCalculo>1.00</BaseCalculo><Aliquota>0.0500</Aliquota><ValorLiquidoNfse>0.55</ValorLiquidoNfse><DescontoIncondicionado>0.50</DescontoIncondicionado></Valores><ItemListaServico>07.01</ItemListaServico><CodigoCnae>0111301</CodigoCnae><CodigoTributacaoMunicipio>1412</CodigoTributacaoMunicipio><Discriminacao>DISCRIMINACAO DO SERVICO DISCRIMINACAO DO SERVICO DISCRIMINACAO DO SERVICO DISCRIMINACAO DO SERVICO DISCRIMINACAO DO SERVICO DISCRIMINACAO DO SERVICO DISCRIMINACAO DO SERVICO DISCRIMINACAO DO SERVICO DISCRIMINACAO DO SERVICO DISCRIMINACAO DO SERVICO DISCRIMINACAO DO SERVICO DISCRIMINACAO DO SERVICO DISCRIMINACAO DO SERVICO DISCRIMINACAO DO SERVICO DISCRIMINACAO DO SERVICO DISCRIMINACAO DO SERVICO DISCRIMINACAO DO SERVICO DISCRIMINACAO DO SERVICOD</Discriminacao><CodigoMunicipio>4117909</CodigoMunicipio></Servico><Prestador><Cnpj>00000000000000</Cnpj><InscricaoMunicipal>00000</InscricaoMunicipal></Prestador><Tomador><IdentificacaoTomador><CpfCnpj><Cnpj>01109184000438</Cnpj></CpfCnpj></IdentificacaoTomador><RazaoSocial>RAZAO SOCIAL DO TOMADOR RAZAO SOCIAL DO TOMADOR RAZAO SOCIAL DO TOMADOR RAZAO SOCIAL DO TOMADOR RAZAO SOCIAL DO TOMADOR RAZAO SOCIAL DO TOMADOR RAZAOS</RazaoSocial><Endereco><Endereco>ENDERECO DO TOMADOR ENDERECO DO TOMADOR ENDERECO DO TOMADOR ENDERECO DO TOMADOR ENDERECO DO TOMADORe</Endereco><Numero>1234567890</Numero><Complemento>COMPLEMENTO DO ENDERECO COMPLEMENTO DO ENDERECO COMPLEMENTOC</Complemento><Bairro>BAIRRO DO TOMADOR BAIRRO DO TOMADOR BAIRRO DO TOMADOR BAIRRO</Bairro><CodigoMunicipio>3543303</CodigoMunicipio><Uf>SP</Uf><Cep>09400000</Cep></Endereco><Contato><Telefone>12345678901234567890</Telefone><Email>ricardo.montanari@senior.com.br</Email></Contato></Tomador><IntermediarioServico><RazaoSocial>RAZAO SOCIAL INTERMEDIARIO RAZAO SOCIAL INTERMEDIARIO RAZAO SOCIAL INTERMEDIARIO RAZAO SOCIAL INTERMEDIARIO RAZAO S</RazaoSocial><CpfCnpj><Cnpj>07236167000103</Cnpj></CpfCnpj><InscricaoMunicipal>36400</InscricaoMunicipal></IntermediarioServico><ContrucaoCivil><CodigoObra>123456789012345</CodigoObra><Art>543210987654321</Art></ContrucaoCivil></InfRps><InfSenior><Tomador><Nome>NOME DO TOMADOR NOME DO TOMADOR NOME DO TOMADOR NOME DO TOMADOR NOME DO TOMADOR NOME DO TOMADOR NOME</Nome><Endereco>ENDERECO DO TOMADOR ENDERECO DO TOMADOR ENDERECO DO TOMADOR ENDERECO DO TOMADOR ENDERECO DO TOMADORe</Endereco><Numero>1234567890</Numero><Bairro>BAIRRO DO TOMADOR BAIRRO DO TOMADOR BAIRRO DO TOMADOR BAIRRO</Bairro><NomeMunicipio>RIBEIRAO PIRES</NomeMunicipio><CodigoMunicipio>3543303</CodigoMunicipio><Uf>SP</Uf><CodigoPais>1058</CodigoPais><IE>ISENTO</IE><DescricaoPaisTomador>BRASIL</DescricaoPaisTomador><NumeroIdentificacaoFiscal>12345</NumeroIdentificacaoFiscal></Tomador><Prestador><NomeFilial>PRESTADOR DO SERVICO - FILIAL</NomeFilial><Endereco>RUA DO PRESTADOR DO SERVICO</Endereco><Numero>1000</Numero><CEP>85950000</CEP><Bairro>BAIRRO DO PRESTADOR DO SERVICO</Bairro><UF>PR</UF><Email>ricardo.montanari@senior.com.br</Email><PaisPrestacaoServico>BRASIL</PaisPrestacaoServico></Prestador><Itens><Item><Descricao>DESCRICAO DO ITEM DE SERVICO 1</Descricao><ValorBruto>1.00</ValorBruto><ValorLiquido>0.55</ValorLiquido><ValorUnitario>1.00000</ValorUnitario><ObservacaoItem/><Quantidade>1.00000</Quantidade><ValorIssRetido>0.00</ValorIssRetido><ValorDeducao>0.00</ValorDeducao><PercentualDeducao>0.00</PercentualDeducao><ValorBaseIss>1.00</ValorBaseIss><ValorDesconto>0.50</ValorDesconto><Codigo>42</Codigo><CodigoTributacao/><ItemListaServico>14.01</ItemListaServico><Unidade>UN</Unidade><pISS>5.00</pISS><vISS>0.05</vISS><bINSS>0.00</bINSS><pINSS>0.00</pINSS><vINSS>0.00</vINSS><bIRRF>0.00</bIRRF><pIRRF>0.00</pIRRF><vIRRF>0.00</vIRRF><bCONFINSret>0.00</bCONFINSret><pCOFINSret>0.00</pCOFINSret><vCOFINSret>0.00</vCOFINSret><bCSLLret>0.00</bCSLLret><pCSLLret>0.00</pCSLLret><vCSLLret>0.00</vCSLLret><bPISret>0.00</bPISret><pPISret>0.00</pPISret><vPISRet>0.00</vPISRet></Item></Itens><Faturas><Fatura><Numero>10003001</Numero><Vencimento>2017-04-17T00:00:00</Vencimento><Valor>1.05</Valor></Fatura></Faturas><Transacao><TipoRecolhimento>A</TipoRecolhimento><MunicipioIncidencia>4202404</MunicipioIncidencia><ValorCargaTributaria>1330.00</ValorCargaTributaria><PercentualCargaTributaria>36.00</PercentualCargaTributaria><AliquotaPIS>0.0000</AliquotaPIS><AliquotaCOFINS>0.0000</AliquotaCOFINS><AliquotaINSS>0.0000</AliquotaINSS><AliquotaIR>0.0000</AliquotaIR><AliquotaCSLL>0.0000</AliquotaCSLL><CodigoNbs>112012300</CodigoNbs></Transacao><Observacao>OBSERVACOES COMPLEMENTARES PARA USO DO PRESTADOR DE SERVICO CONFORME REGULAMENTO DO MUNICIPIO</Observacao></InfSenior></Rps></ListaRps></LoteRps></EnviarLoteRpsEnvio>";
        }

        private string XmlCte()
        {
            return "<?xml version=\"1.0\" encoding=\"utf-8\"?><CTe xmlns=\"http://www.portalfiscal.inf.br/cte\"><infCte Id=\"CTe42180980680093000181570100000092261000000000\" versao=\"3.00\"><ide><cUF>42</cUF><cCT>00000000</cCT><CFOP>5932</CFOP><natOp>Lancamento Ef.Tit.Reclass.Merc.Dec.Kit</natOp><mod>57</mod><serie>10</serie><nCT>9226</nCT><dhEmi>2018-09-01T15:34:00-03:00</dhEmi><tpImp>1</tpImp><tpEmis>1</tpEmis><cDV>0</cDV><tpAmb>2</tpAmb><tpCTe>0</tpCTe><procEmi>0</procEmi><verProc>5.8.9.1</verProc><cMunEnv>4202404</cMunEnv><xMunEnv>BLUMENAU</xMunEnv><UFEnv>SC</UFEnv><modal>01</modal><tpServ>0</tpServ><cMunIni>4202404</cMunIni><xMunIni>BLUMENAU</xMunIni><UFIni>SC</UFIni><cMunFim>4208203</cMunFim><xMunFim>ITAJAI</xMunFim><UFFim>SC</UFFim><retira>1</retira><indIEToma>1</indIEToma><toma3><toma>0</toma></toma3></ide><compl><xEmi>suporte</xEmi><Entrega><semData><tpPer>0</tpPer></semData><semHora><tpHor>0</tpHor></semHora></Entrega><origCalc>ITAJAI</origCalc><destCalc>ITAJAI</destCalc><xObs>OBSERVACOES GERAIS OBSERVACOES GERAIS OBSERVACOES GERAIS OBSERVACOES GERAIS OBSERVACOES GERAIS OBSERVACOES GERAIS OBSERVACOES GERAIS OBSERVACOES GERAIS OBSERVACOES GERAIS OBSERVACOES GERAIS OBSERVACOES GERAIS OBSERVACOES GERAIS OBSERVACOES GERAIS OBS</xObs></compl><emit><CNPJ>80680093000181</CNPJ><IE>252812441</IE><xNome>NOME DO EMITENTE DO CTE NOME DO EMITENTE DO CTE NOME DO EMIT</xNome><xFant>NOME FANTASIA DO EMITENTE DO CTE NOME FANTASIA DO EMITENTE D</xFant><enderEmit><xLgr>LOGRADOURO DO EMITENTE DO CTE LOGRADOURO DO EMITENTE DO CTEL</xLgr><nro>10825</nro><xCpl>COMPLEMENTO DO ENDERECO DO EMITENTE DO CTE COMPLEMENTO DO EN</xCpl><xBairro>BAIRRO DO ENDERECO DO EMITENTE DO CTE BAIRRO DO ENDERECO DOB</xBairro><cMun>4202404</cMun><xMun>BLUMENAU</xMun><CEP>89012530</CEP><UF>SC</UF><fone>00047333403300</fone></enderEmit></emit><rem><CNPJ>80680093000858</CNPJ><IE>353160510113</IE><xNome>CT-E EMITIDO EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL</xNome><xFant>CT-E EMITIDO EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FANTASIA</xFant><fone>00004788445566</fone><enderReme><xLgr>LOGRADOURO DO REMETENTE LOGRADOURO DO REMETENTE LOGRADOURO D</xLgr><nro>99123</nro><xCpl>COMPLEMENTO DO ENDERECO DO REMETENTE COMPLEMENTO DO ENDERECO</xCpl><xBairro>BAIRRO DO ENDERECO DO REMETENTE BAIRRO DO ENDERECO DO REMETE</xBairro><cMun>3550308</cMun><xMun>SAO PAULO</xMun><CEP>08000000</CEP><UF>SP</UF><cPais>1058</cPais><xPais>Brasil</xPais></enderReme><email>ricardo.montanari@senior.com.br</email></rem><exped><CNPJ>76497338002378</CNPJ><IE>78219785</IE><xNome>CT-E EMITIDO EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL</xNome><fone>00004733335588</fone><enderExped><xLgr>LOGRADOURO DO EXPEDIDOR LOGRADOURO DO EXPEDIDOR LOGRADOURO D</xLgr><nro>98123</nro><xCpl>COMPLEMENTO DO ENDERECO DO EXPEDIDOR COMPLEMENTO DO ENDERECO</xCpl><xBairro>BAIRRO DO ENDERECO DO EXPEDIDOR BAIRRO DO ENDERECO DO EXPEDI</xBairro><cMun>3304557</cMun><xMun>Rio de Janeiro</xMun><CEP>78705000</CEP><UF>RJ</UF><cPais>1058</cPais><xPais>BRASIL</xPais></enderExped><email>ricardo.montanari@senior.com.br</email></exped><receb><CNPJ>83054437000305</CNPJ><IE>283628707</IE><xNome>CT-E EMITIDO EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL</xNome><fone>00004733335588</fone><enderReceb><xLgr>LOGRADOURO DO ENDERECO DO RECEBEDOR LOGRADOURO DO ENDERECO D</xLgr><nro>88123</nro><xCpl>COMPLEMENTO DO ENDERECO DO RECEBEDOR COMPLEMENTO DO ENDERECO</xCpl><xBairro>BAIRRO DO ENDERECO DO RECEBEDOR BAIRRO DO ENDERECO DO RECEBE</xBairro><cMun>5006200</cMun><xMun>Nova Andradina</xMun><CEP>79750000</CEP><UF>MS</UF><cPais>1058</cPais><xPais>BRASIL</xPais></enderReceb><email>jean.bastos@senior.com.br</email></receb><dest><CNPJ>01979502000179</CNPJ><IE>1610058698</IE><xNome>CT-E EMITIDO EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL</xNome><fone>05555555555</fone><enderDest><xLgr>LOGRADOURO DO DESTINATARIO LOGRADOURO DO DESTINATARIO LOGRAD</xLgr><nro>77123</nro><xCpl>COMPLEMENTO DO ENDERECO DO DESTINATARIO COMPLEMENTO DO ENDER</xCpl><xBairro>BAIRRO DO ENDERECO DO DESTINATARIO BAIRRO DO ENDERECO DO DES</xBairro><cMun>4310108</cMun><xMun>IGREJINHA</xMun><CEP>95650000</CEP><UF>RS</UF><cPais>1058</cPais><xPais>Brasil</xPais></enderDest><email>sergio.tomio@senior.com.br</email></dest><vPrest><vTPrest>102.00</vTPrest><vRec>100.00</vRec><Comp><xNome>NOME DO COMPON1</xNome><vComp>100.00</vComp></Comp><Comp><xNome>NOME DO COMPON2</xNome><vComp>200.00</vComp></Comp><Comp><xNome>NOME DO COMPON3</xNome><vComp>300.00</vComp></Comp><Comp><xNome>NOME DO COMPON4</xNome><vComp>400.00</vComp></Comp><Comp><xNome>NOME DO COMPON5</xNome><vComp>400.00</vComp></Comp><Comp><xNome>NOME DO COMPON6</xNome><vComp>400.00</vComp></Comp><Comp><xNome>NOME DO COMPON7</xNome><vComp>400.00</vComp></Comp><Comp><xNome>NOME DO COMPON8</xNome><vComp>400.00</vComp></Comp><Comp><xNome>NOME DO COMPON9</xNome><vComp>400.00</vComp></Comp><Comp><xNome>NOME DO COMPO10</xNome><vComp>400.00</vComp></Comp></vPrest><imp><ICMS><ICMS45><CST>51</CST></ICMS45></ICMS></imp><infCTeNorm><infCarga><vCarga>1000.00</vCarga><proPred>PRODUTO PREDOMINANTE</proPred><xOutCat>OUTRAS CARACTERISTICAS DA CARG</xOutCat><infQ><cUnid>03</cUnid><tpMed>TESTE</tpMed><qCarga>1</qCarga></infQ></infCarga><infDoc><infNF><nRoma>12345678901234567890</nRoma><nPed>09876543210987654321</nPed><mod>01</mod><serie>1</serie><nDoc>98765432112345678901</nDoc><dEmi>2017-03-02</dEmi><vBC>100.00</vBC><vICMS>17.00</vICMS><vBCST>150.00</vBCST><vST>25.50</vST><vProd>155.00</vProd><vNF>1055.00</vNF><nCFOP>5932</nCFOP><dPrev>2017-03-02</dPrev></infNF><infNF><nRoma>12345678901234567890</nRoma><nPed>09876543210987654321</nPed><mod>01</mod><serie>2</serie><nDoc>98765432112345678901</nDoc><dEmi>2017-03-02</dEmi><vBC>100.00</vBC><vICMS>17.00</vICMS><vBCST>150.00</vBCST><vST>25.50</vST><vProd>155.00</vProd><vNF>1055.00</vNF><nCFOP>5932</nCFOP><dPrev>2017-03-02</dPrev></infNF><infNF><nRoma>12345678901234567890</nRoma><nPed>09876543210987654321</nPed><mod>01</mod><serie>3</serie><nDoc>98765432112345678901</nDoc><dEmi>2017-03-02</dEmi><vBC>100.00</vBC><vICMS>17.00</vICMS><vBCST>150.00</vBCST><vST>25.50</vST><vProd>155.00</vProd><vNF>1055.00</vNF><nCFOP>5932</nCFOP><dPrev>2017-03-02</dPrev></infNF><infNF><nRoma>12345678901234567890</nRoma><nPed>09876543210987654321</nPed><mod>01</mod><serie>4</serie><nDoc>98765432112345678901</nDoc><dEmi>2017-03-02</dEmi><vBC>100.00</vBC><vICMS>17.00</vICMS><vBCST>150.00</vBCST><vST>25.50</vST><vProd>155.00</vProd><vNF>1055.00</vNF><nCFOP>5932</nCFOP><dPrev>2017-03-02</dPrev></infNF><infNF><nRoma>12345678901234567890</nRoma><nPed>09876543210987654321</nPed><mod>01</mod><serie>5</serie><nDoc>98765432112345678901</nDoc><dEmi>2017-03-02</dEmi><vBC>100.00</vBC><vICMS>17.00</vICMS><vBCST>150.00</vBCST><vST>25.50</vST><vProd>155.00</vProd><vNF>1055.00</vNF><nCFOP>5932</nCFOP><dPrev>2017-03-02</dPrev></infNF><infNF><nRoma>12345678901234567890</nRoma><nPed>09876543210987654321</nPed><mod>01</mod><serie>6</serie><nDoc>98765432112345678901</nDoc><dEmi>2017-03-02</dEmi><vBC>100.00</vBC><vICMS>17.00</vICMS><vBCST>150.00</vBCST><vST>25.50</vST><vProd>155.00</vProd><vNF>1055.00</vNF><nCFOP>5932</nCFOP><dPrev>2017-03-02</dPrev></infNF><infNF><nRoma>12345678901234567890</nRoma><nPed>09876543210987654321</nPed><mod>01</mod><serie>7</serie><nDoc>98765432112345678901</nDoc><dEmi>2017-03-02</dEmi><vBC>100.00</vBC><vICMS>17.00</vICMS><vBCST>150.00</vBCST><vST>25.50</vST><vProd>155.00</vProd><vNF>1055.00</vNF><nCFOP>5932</nCFOP><dPrev>2017-03-02</dPrev></infNF><infNF><nRoma>12345678901234567890</nRoma><nPed>09876543210987654321</nPed><mod>01</mod><serie>8</serie><nDoc>98765432112345678901</nDoc><dEmi>2017-03-02</dEmi><vBC>100.00</vBC><vICMS>17.00</vICMS><vBCST>150.00</vBCST><vST>25.50</vST><vProd>155.00</vProd><vNF>1055.00</vNF><nCFOP>5932</nCFOP><dPrev>2017-03-02</dPrev></infNF><infNF><nRoma>12345678901234567890</nRoma><nPed>09876543210987654321</nPed><mod>01</mod><serie>9</serie><nDoc>98765432112345678901</nDoc><dEmi>2017-03-02</dEmi><vBC>100.00</vBC><vICMS>17.00</vICMS><vBCST>150.00</vBCST><vST>25.50</vST><vProd>155.00</vProd><vNF>1055.00</vNF><nCFOP>5932</nCFOP><dPrev>2017-03-02</dPrev></infNF><infNF><nRoma>12345678901234567890</nRoma><nPed>09876543210987654321</nPed><mod>01</mod><serie>10</serie><nDoc>98765432112345678901</nDoc><dEmi>2017-03-02</dEmi><vBC>100.00</vBC><vICMS>17.00</vICMS><vBCST>150.00</vBCST><vST>25.50</vST><vProd>155.00</vProd><vNF>1055.00</vNF><nCFOP>5932</nCFOP><dPrev>2017-03-02</dPrev></infNF><infNF><nRoma>12345678901234567890</nRoma><nPed>09876543210987654321</nPed><mod>01</mod><serie>11</serie><nDoc>98765432112345678901</nDoc><dEmi>2017-03-02</dEmi><vBC>100.00</vBC><vICMS>17.00</vICMS><vBCST>150.00</vBCST><vST>25.50</vST><vProd>155.00</vProd><vNF>1055.00</vNF><nCFOP>5932</nCFOP><dPrev>2017-03-02</dPrev></infNF><infNF><nRoma>12345678901234567890</nRoma><nPed>09876543210987654321</nPed><mod>01</mod><serie>12</serie><nDoc>98765432112345678901</nDoc><dEmi>2017-03-02</dEmi><vBC>100.00</vBC><vICMS>17.00</vICMS><vBCST>150.00</vBCST><vST>25.50</vST><vProd>155.00</vProd><vNF>1055.00</vNF><nCFOP>5932</nCFOP><dPrev>2017-03-02</dPrev></infNF><infNF><nRoma>12345678901234567890</nRoma><nPed>09876543210987654321</nPed><mod>01</mod><serie>13</serie><nDoc>98765432112345678901</nDoc><dEmi>2017-03-02</dEmi><vBC>100.00</vBC><vICMS>17.00</vICMS><vBCST>150.00</vBCST><vST>25.50</vST><vProd>155.00</vProd><vNF>1055.00</vNF><nCFOP>5932</nCFOP><dPrev>2017-03-02</dPrev></infNF><infNF><nRoma>12345678901234567890</nRoma><nPed>09876543210987654321</nPed><mod>01</mod><serie>14</serie><nDoc>98765432112345678901</nDoc><dEmi>2017-03-02</dEmi><vBC>100.00</vBC><vICMS>17.00</vICMS><vBCST>150.00</vBCST><vST>25.50</vST><vProd>155.00</vProd><vNF>1055.00</vNF><nCFOP>5932</nCFOP><dPrev>2017-03-02</dPrev></infNF><infNF><nRoma>12345678901234567890</nRoma><nPed>09876543210987654321</nPed><mod>01</mod><serie>15</serie><nDoc>98765432112345678901</nDoc><dEmi>2017-03-02</dEmi><vBC>100.00</vBC><vICMS>17.00</vICMS><vBCST>150.00</vBCST><vST>25.50</vST><vProd>155.00</vProd><vNF>1055.00</vNF><nCFOP>5932</nCFOP><dPrev>2017-03-02</dPrev></infNF><infNF><nRoma>12345678901234567890</nRoma><nPed>09876543210987654321</nPed><mod>01</mod><serie>16</serie><nDoc>98765432112345678901</nDoc><dEmi>2017-03-02</dEmi><vBC>100.00</vBC><vICMS>17.00</vICMS><vBCST>150.00</vBCST><vST>25.50</vST><vProd>155.00</vProd><vNF>1055.00</vNF><nCFOP>5932</nCFOP><dPrev>2017-03-02</dPrev></infNF><infNF><nRoma>12345678901234567890</nRoma><nPed>09876543210987654321</nPed><mod>01</mod><serie>17</serie><nDoc>98765432112345678901</nDoc><dEmi>2017-03-02</dEmi><vBC>100.00</vBC><vICMS>17.00</vICMS><vBCST>150.00</vBCST><vST>25.50</vST><vProd>155.00</vProd><vNF>1055.00</vNF><nCFOP>5932</nCFOP><dPrev>2017-03-02</dPrev></infNF><infNF><nRoma>12345678901234567890</nRoma><nPed>09876543210987654321</nPed><mod>01</mod><serie>18</serie><nDoc>98765432112345678901</nDoc><dEmi>2017-03-02</dEmi><vBC>100.00</vBC><vICMS>17.00</vICMS><vBCST>150.00</vBCST><vST>25.50</vST><vProd>155.00</vProd><vNF>1055.00</vNF><nCFOP>5932</nCFOP><dPrev>2017-03-02</dPrev></infNF><infNF><nRoma>12345678901234567890</nRoma><nPed>09876543210987654321</nPed><mod>01</mod><serie>19</serie><nDoc>98765432112345678901</nDoc><dEmi>2017-03-02</dEmi><vBC>100.00</vBC><vICMS>17.00</vICMS><vBCST>150.00</vBCST><vST>25.50</vST><vProd>155.00</vProd><vNF>1055.00</vNF><nCFOP>5932</nCFOP><dPrev>2017-03-02</dPrev></infNF><infNF><nRoma>12345678901234567890</nRoma><nPed>09876543210987654321</nPed><mod>01</mod><serie>20</serie><nDoc>98765432112345678901</nDoc><dEmi>2017-03-02</dEmi><vBC>100.00</vBC><vICMS>17.00</vICMS><vBCST>150.00</vBCST><vST>25.50</vST><vProd>155.00</vProd><vNF>1055.00</vNF><nCFOP>5932</nCFOP><dPrev>2017-03-02</dPrev></infNF><infNF><nRoma>12345678901234567890</nRoma><nPed>09876543210987654321</nPed><mod>01</mod><serie>21</serie><nDoc>98765432112345678901</nDoc><dEmi>2017-03-02</dEmi><vBC>100.00</vBC><vICMS>17.00</vICMS><vBCST>150.00</vBCST><vST>25.50</vST><vProd>155.00</vProd><vNF>1055.00</vNF><nCFOP>5932</nCFOP><dPrev>2017-03-02</dPrev></infNF><infNF><nRoma>12345678901234567890</nRoma><nPed>09876543210987654321</nPed><mod>01</mod><serie>22</serie><nDoc>98765432112345678901</nDoc><dEmi>2017-03-02</dEmi><vBC>100.00</vBC><vICMS>17.00</vICMS><vBCST>150.00</vBCST><vST>25.50</vST><vProd>155.00</vProd><vNF>1055.00</vNF><nCFOP>5932</nCFOP><dPrev>2017-03-02</dPrev></infNF><infNF><nRoma>12345678901234567890</nRoma><nPed>09876543210987654321</nPed><mod>01</mod><serie>23</serie><nDoc>98765432112345678901</nDoc><dEmi>2017-03-02</dEmi><vBC>100.00</vBC><vICMS>17.00</vICMS><vBCST>150.00</vBCST><vST>25.50</vST><vProd>155.00</vProd><vNF>1055.00</vNF><nCFOP>5932</nCFOP><dPrev>2017-03-02</dPrev></infNF><infNF><nRoma>12345678901234567890</nRoma><nPed>09876543210987654321</nPed><mod>01</mod><serie>24</serie><nDoc>98765432112345678901</nDoc><dEmi>2017-03-02</dEmi><vBC>100.00</vBC><vICMS>17.00</vICMS><vBCST>150.00</vBCST><vST>25.50</vST><vProd>155.00</vProd><vNF>1055.00</vNF><nCFOP>5932</nCFOP><dPrev>2017-03-02</dPrev></infNF><infNF><nRoma>12345678901234567890</nRoma><nPed>09876543210987654321</nPed><mod>01</mod><serie>25</serie><nDoc>98765432112345678901</nDoc><dEmi>2017-03-02</dEmi><vBC>100.00</vBC><vICMS>17.00</vICMS><vBCST>150.00</vBCST><vST>25.50</vST><vProd>155.00</vProd><vNF>1055.00</vNF><nCFOP>5932</nCFOP><dPrev>2017-03-02</dPrev></infNF><infNF><nRoma>12345678901234567890</nRoma><nPed>09876543210987654321</nPed><mod>01</mod><serie>26</serie><nDoc>98765432112345678901</nDoc><dEmi>2017-03-02</dEmi><vBC>100.00</vBC><vICMS>17.00</vICMS><vBCST>150.00</vBCST><vST>25.50</vST><vProd>155.00</vProd><vNF>1055.00</vNF><nCFOP>5932</nCFOP><dPrev>2017-03-02</dPrev></infNF><infNF><nRoma>12345678901234567890</nRoma><nPed>09876543210987654321</nPed><mod>01</mod><serie>27</serie><nDoc>98765432112345678901</nDoc><dEmi>2017-03-02</dEmi><vBC>100.00</vBC><vICMS>17.00</vICMS><vBCST>150.00</vBCST><vST>25.50</vST><vProd>155.00</vProd><vNF>1055.00</vNF><nCFOP>5932</nCFOP><dPrev>2017-03-02</dPrev></infNF><infNF><nRoma>22345678901234567890</nRoma><nPed>09876543210987654322</nPed><mod>01</mod><serie>28</serie><nDoc>98765432112345678902</nDoc><dEmi>2018-04-04</dEmi><vBC>100.00</vBC><vICMS>17.00</vICMS><vBCST>150.00</vBCST><vST>25.50</vST><vProd>155.00</vProd><vNF>1055.00</vNF><nCFOP>5932</nCFOP><dPrev>2017-03-02</dPrev></infNF><infNF><nRoma>22345278901234567890</nRoma><nPed>09876523210987654322</nPed><mod>01</mod><serie>29</serie><nDoc>98765432112345678902</nDoc><dEmi>2018-04-04</dEmi><vBC>100.00</vBC><vICMS>17.00</vICMS><vBCST>150.00</vBCST><vST>25.50</vST><vProd>155.00</vProd><vNF>1055.00</vNF><nCFOP>5932</nCFOP><dPrev>2017-03-02</dPrev></infNF><infNF><nRoma>22345278901234567890</nRoma><nPed>09876523210987654322</nPed><mod>01</mod><serie>30</serie><nDoc>98765432112345678902</nDoc><dEmi>2018-04-04</dEmi><vBC>100.00</vBC><vICMS>17.00</vICMS><vBCST>150.00</vBCST><vST>25.50</vST><vProd>155.00</vProd><vNF>1055.00</vNF><nCFOP>5932</nCFOP><dPrev>2017-03-02</dPrev></infNF><infNF><nRoma>22345278901234567890</nRoma><nPed>09876523210987654322</nPed><mod>01</mod><serie>31</serie><nDoc>98765432112345678902</nDoc><dEmi>2018-04-04</dEmi><vBC>100.00</vBC><vICMS>17.00</vICMS><vBCST>150.00</vBCST><vST>25.50</vST><vProd>155.00</vProd><vNF>1055.00</vNF><nCFOP>5932</nCFOP><dPrev>2017-03-02</dPrev></infNF><infNF><nRoma>22345278901234567890</nRoma><nPed>09876523210987654322</nPed><mod>01</mod><serie>32</serie><nDoc>98765432112345678902</nDoc><dEmi>2018-04-04</dEmi><vBC>100.00</vBC><vICMS>17.00</vICMS><vBCST>150.00</vBCST><vST>25.50</vST><vProd>155.00</vProd><vNF>1055.00</vNF><nCFOP>5932</nCFOP><dPrev>2017-03-02</dPrev></infNF><infNF><nRoma>22345278901234567890</nRoma><nPed>09876523210987654322</nPed><mod>01</mod><serie>33</serie><nDoc>98765432112345678902</nDoc><dEmi>2018-04-04</dEmi><vBC>100.00</vBC><vICMS>17.00</vICMS><vBCST>150.00</vBCST><vST>25.50</vST><vProd>155.00</vProd><vNF>1055.00</vNF><nCFOP>5932</nCFOP><dPrev>2017-03-02</dPrev></infNF><infNF><nRoma>22345278901234567890</nRoma><nPed>09876523210987654322</nPed><mod>01</mod><serie>34</serie><nDoc>98765432112345678902</nDoc><dEmi>2018-04-04</dEmi><vBC>100.00</vBC><vICMS>17.00</vICMS><vBCST>150.00</vBCST><vST>25.50</vST><vProd>155.00</vProd><vNF>1055.00</vNF><nCFOP>5932</nCFOP><dPrev>2017-03-02</dPrev></infNF><infNF><nRoma>09876543210987654321</nRoma><nPed>12345678901234567890</nPed><mod>01</mod><serie>35</serie><nDoc>11111111111111111111</nDoc><dEmi>2017-03-01</dEmi><vBC>101.00</vBC><vICMS>18.00</vICMS><vBCST>151.00</vBCST><vST>26.50</vST><vProd>156.00</vProd><vNF>1056.00</vNF><nCFOP>5932</nCFOP><dPrev>2017-03-06</dPrev><infUnidTransp><tpUnidTransp>1</tpUnidTransp><idUnidTransp>12345678901234567890</idUnidTransp><lacUnidTransp><nLacre>11223344556677889900</nLacre></lacUnidTransp><infUnidCarga><tpUnidCarga>1</tpUnidCarga><idUnidCarga>11111111111111111111</idUnidCarga><lacUnidCarga><nLacre>22222222222222222222</nLacre></lacUnidCarga><qtdRat>800.00</qtdRat></infUnidCarga><qtdRat>333.55</qtdRat></infUnidTransp><infUnidTransp><tpUnidTransp>7</tpUnidTransp><idUnidTransp>98765432109876543210</idUnidTransp><lacUnidTransp><nLacre>33333333333333333333</nLacre></lacUnidTransp><infUnidCarga><tpUnidCarga>3</tpUnidCarga><idUnidCarga>44444444444444444444</idUnidCarga><lacUnidCarga><nLacre>55555555555555555555</nLacre></lacUnidCarga><qtdRat>801.00</qtdRat></infUnidCarga><qtdRat>331.55</qtdRat></infUnidTransp></infNF></infDoc><infModal versaoModal=\"3.00\"><rodo><RNTRC>12345678</RNTRC><occ><serie>123</serie><nOcc>123456</nOcc><dEmi>2017-03-02</dEmi><emiOcc><CNPJ>02012862002537</CNPJ><cInt>1234567890</cInt><IE>030203198</IE><UF>SP</UF><fone>5547988556633</fone></emiOcc></occ></rodo></infModal><cobr><fat><nFat>123456789012345678901234567890123456789012345678901234567890</nFat><vOrig>10000.52</vOrig><vDesc>0.52</vDesc><vLiq>10000.00</vLiq></fat><dup><nDup>111111111122222222223333333333444444444455555555556666666666</nDup><dVenc>2017-04-01</dVenc><vDup>50000.00</vDup></dup><dup><nDup>777777777788888888889999999999444444444455555555556666666666</nDup><dVenc>2017-05-01</dVenc><vDup>50000.01</vDup></dup></cobr></infCTeNorm><autXML><CNPJ>99999999000191</CNPJ></autXML><autXML><CPF>03413729861</CPF></autXML></infCte></CTe>";
        }

        private string XmlMdfe()
        {
            return "<?xml version=\"1.0\" encoding=\"utf-8\"?><MDFe xmlns=\"http://www.portalfiscal.inf.br/mdfe\"><infMDFe Id=\"MDFe42190180680093000181580100008855741000000002\" versao=\"3.00\"><ide><cUF>42</cUF><tpAmb>2</tpAmb><tpEmit>1</tpEmit><mod>58</mod><serie>10</serie><nMDF>885574</nMDF><cMDF>00000000</cMDF><cDV>2</cDV><modal>1</modal><dhEmi>2019-01-04T13:09:00-02:00</dhEmi><tpEmis>1</tpEmis><procEmi>0</procEmi><verProc>5.8.10.73</verProc><UFIni>SC</UFIni><UFFim>RS</UFFim><infMunCarrega><cMunCarrega>4202404</cMunCarrega><xMunCarrega>BLUMENAU</xMunCarrega></infMunCarrega><dhIniViagem>2019-01-04T13:09:00-02:00</dhIniViagem></ide><emit><CNPJ>80680093000181</CNPJ><IE>252812441</IE><xNome>Senior Sistemas</xNome><xFant>Senior Sistemas - SC</xFant><enderEmit><xLgr>Rua São Paulo</xLgr><nro>1</nro><xBairro>Centro</xBairro><cMun>4202404</cMun><xMun>BLUMENAU</xMun><CEP>89012001</CEP><UF>SC</UF><fone>3735121050</fone><email>graziany.fernandes@senior.com.br</email></enderEmit></emit><infModal versaoModal=\"3.00\"><rodo><infANTT><RNTRC>00775625</RNTRC><infContratante><CNPJ>07358761005802</CNPJ></infContratante></infANTT><veicTracao><placa>HDS8942</placa><RENAVAM>234461063</RENAVAM><tara>17000</tara><capKG>39000</capKG><capM3>0</capM3><prop><CNPJ>04784896000106</CNPJ><RNTRC>00775625</RNTRC><xNome>TRANSCODIL LTDA</xNome><IE>2231570690074</IE><UF>MG</UF><tpProp>0</tpProp></prop><condutor><xNome>VICENTE GOMES FERREIRA</xNome><CPF>31259855600</CPF></condutor><tpRod>03</tpRod><tpCar>03</tpCar><UF>MG</UF></veicTracao><veicReboque><placa>HBN9380</placa><RENAVAM>875772560</RENAVAM><tara>38000</tara><capKG>38000</capKG><capM3>0</capM3><prop><CNPJ>04784896000106</CNPJ><RNTRC>00775625</RNTRC><xNome>TRANSCODIL LTDA</xNome><IE>2231570690074</IE><UF>MG</UF><tpProp>0</tpProp></prop><tpCar>03</tpCar><UF>MG</UF></veicReboque><veicReboque><placa>HCU1413</placa><RENAVAM>875774644</RENAVAM><tara>38000</tara><capKG>38000</capKG><capM3>0</capM3><prop><CNPJ>04784896000106</CNPJ><RNTRC>00775526</RNTRC><xNome>TRANSCODIL LTDA</xNome><IE>2231570690074</IE><UF>MG</UF><tpProp>0</tpProp></prop><tpCar>03</tpCar><UF>MG</UF></veicReboque><veicReboque><placa>HCU1314</placa><RENAVAM>875774687</RENAVAM><tara>38000</tara><capKG>38000</capKG><capM3>0</capM3><tpCar>03</tpCar><UF>MG</UF></veicReboque></rodo></infModal><infDoc><infMunDescarga><cMunDescarga>4305355</cMunDescarga><xMunDescarga>CHARQUEADAS</xMunDescarga><infCTe><chCTe>31180804784896000106570010000822511000822813</chCTe></infCTe></infMunDescarga></infDoc><seg><infResp><respSeg>1</respSeg><CNPJ>04784896000106</CNPJ></infResp><infSeg><xSeg>SEGUROS SURA SA</xSeg><CNPJ>33065699000127</CNPJ></infSeg><nApol>2100040369</nApol><nAver>0675109180478489600010657001000082251147</nAver></seg><tot><qCTe>1</qCTe><vCarga>51479.90</vCarga><cUnid>01</cUnid><qCarga>37130.0000</qCarga></tot></infMDFe></MDFe>";
        }

        private string XmlNfce()
        {
            return "<?xml version=\"1.0\" encoding=\"utf-8\"?><NFe xmlns=\"http://www.portalfiscal.inf.br/nfe\"><infNFe versao=\"4.00\" Id=\"NFe35190180680093000858650100008855741000000009\"><ide><cUF>35</cUF><cNF>00000000</cNF><natOp>Venda Mercad.Adquir.e/ou Receb.Terceiros</natOp><mod>65</mod><serie>10</serie><nNF>885574</nNF><dhEmi>2019-01-04T13:40:00-02:00</dhEmi><tpNF>1</tpNF><idDest>1</idDest><cMunFG>3520509</cMunFG><tpImp>4</tpImp><tpEmis>1</tpEmis><cDV>9</cDV><tpAmb>2</tpAmb><finNFe>1</finNFe><indFinal>1</indFinal><indPres>1</indPres><procEmi>0</procEmi><verProc>5.8.5.17</verProc></ide><emit><CNPJ>80680093000858</CNPJ><xNome>Senior SP</xNome><xFant>Senior SP</xFant><enderEmit><xLgr>Rua TESTE SENIOR SP</xLgr><nro>825</nro><xCpl>COMPLEMENTO</xCpl><xBairro>CENTRO</xBairro><cMun>3520509</cMun><xMun>SAO PAULO</xMun><UF>SP</UF><CEP>08000000</CEP><cPais>1058</cPais><xPais>Brasil</xPais><fone>4733403300</fone></enderEmit><IE>353160510113</IE><IM>ISENTO</IM><CNAE>6504000</CNAE><CRT>3</CRT></emit><dest><CNPJ>95143514000102</CNPJ><xNome>NF-E EMITIDA EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL</xNome><enderDest><xLgr>Rua dos testes</xLgr><nro>123</nro><xCpl>1234</xCpl><xBairro>Centro</xBairro><cMun>4202404</cMun><xMun>BLUMENAU</xMun><UF>SC</UF><CEP>89000000</CEP><cPais>1058</cPais><xPais>Brasil</xPais></enderDest><indIEDest>9</indIEDest></dest> <entrega><CNPJ>09744607000150</CNPJ><xLgr>LOGRADOURO DO LOCAL DE ENTREGA LOGRADOURO DO LOCAL DE ENTREG</xLgr><nro>987654</nro><xCpl>COMPLEMENTO DO ENDERECO DO LOCAL DE ENTREGA COMPLEMENTO DO E</xCpl><xBairro>BAIRRO DO ENDERECO DO LOCAL DE ENTREGA BAIRRO DO ENDERECO DO</xBairro><cMun>4207502</cMun><xMun>INDAIAL</xMun><UF>SC</UF></entrega><det nItem=\"1\"><prod><cProd>COMB01</cProd><cEAN>SEM GTIN</cEAN><xProd>NOTA FISCAL EMITIDA EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL</xProd><NCM>27101259</NCM><CEST>0600203</CEST><CFOP>5102</CFOP><uCom>ML</uCom><qCom>1.0000</qCom><vUnCom>199000.0000000000</vUnCom><vProd>199000.00</vProd><cEANTrib>SEM GTIN</cEANTrib><uTrib>ML</uTrib><qTrib>1.0000</qTrib><vUnTrib>199000.0000000000</vUnTrib><indTot>1</indTot><comb><cProdANP>210302004</cProdANP><descANP>DIESEL B20 INTERIOR ADITIVADO</descANP><UFCons>SC</UFCons></comb></prod><imposto><ICMS><ICMS90><orig>0</orig><CST>90</CST><modBC>3</modBC><vBC>0.00</vBC><pICMS>12.00</pICMS><vICMS>0.00</vICMS></ICMS90></ICMS></imposto></det><total><ICMSTot><vBC>0.00</vBC><vICMS>0.00</vICMS><vICMSDeson>0.00</vICMSDeson><vFCP>0.00</vFCP><vBCST>0.00</vBCST><vST>0.00</vST><vFCPST>0.00</vFCPST><vFCPSTRet>0.00</vFCPSTRet><vProd>199000.00</vProd><vFrete>0.00</vFrete><vSeg>0.00</vSeg><vDesc>0.00</vDesc><vII>0.00</vII><vIPI>0.00</vIPI><vIPIDevol>0.00</vIPIDevol><vPIS>0.00</vPIS><vCOFINS>0.00</vCOFINS><vOutro>0.00</vOutro><vNF>199000.00</vNF></ICMSTot></total><transp><modFrete>9</modFrete></transp><pag><detPag><tPag>01</tPag><vPag>200000.00</vPag></detPag><vTroco>1000.00</vTroco></pag><infAdic><infAdFisco>Qualquer coisa</infAdFisco></infAdic></infNFe></NFe>";
        }

        private string GerarDigitoVerificadorNFe(string chave)
        {
            int soma = 0; // Vai guardar a Soma
            int mod = -1; // Vai guardar o Resto da divisão
            int dv = -1;  // Vai guardar o DigitoVerificador
            int pesso = 2; // vai guardar o pesso de multiplicacao

            //percorrendo cada caracter da chave da direita para esquerda para fazer os calculos com o pesso
            for (int i = chave.Length - 1; i != -1; i--)
            {
                int ch = Convert.ToInt32(chave[i].ToString());
                soma += ch * pesso;
                //sempre que for 9 voltamos o pesso a 2
                if (pesso < 9)
                    pesso += 1;
                else
                    pesso = 2;
            }

            //Agora que tenho a soma vamos pegar o resto da divisão por 11
            mod = soma % 11;
            //Aqui temos uma regrinha, se o resto da divisão for 0 ou 1 então o dv vai ser 0
            if (mod == 0 || mod == 1)
                dv = 0;
            else
                dv = 11 - mod;

            return dv.ToString();
        }

        private void btnSelecionarDiretorio_Click(object sender, EventArgs e)
        {
            var diretorio = new FolderBrowserDialog();
            diretorio.Description = "Selecione o diretorio a ser salvo";
            diretorio.ShowDialog();
            txtDiretorio.Text = diretorio.SelectedPath.ToString();
        }

        private void btnXmlUtilizado_Click(object sender, EventArgs e)
        {
            var documento = new OpenFileDialog();
            documento.Title = "Selecione o documento a ser utilizado";
            documento.ShowDialog();
            txbXmlUtilizado.Text = documento.FileName.ToString();
        }
    }
}
